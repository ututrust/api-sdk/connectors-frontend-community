# Connectors

Welcome to the UTU bounty programme for creating social media connectors.  

The UTU Trust Network is about knowing if comments or endorsements from others on a web asset, 
are from people connected to the user’s social media networks or not.  For example, a comment 
coming from a linkedin connected contact carries more weight than a comment coming from a random 
unverified person.   This effort helps eliminate spam and miss-information coming from bots and 
malicious users.

The social media connectors are a valuable part of the UTU Trust SDK. They allow a user to log into
a social media platform and allow the UTU SDK to retrieve the User's connections.  These 
connections allow the app to tell the user if some comments / signal for a certain resource are
coming from someone in their network or not.

In an inniative to encourage the community to get involved with the UTU Trust Network, UTU is
providing bounty in return for creating a connector.  The code for the connector has 2 software
components:

(i) Frontend hosted at: https://gitlab.com/ututrust/api-sdk/connectors-frontend-community.git

(ii) Backend hosted at:  https://gitlab.com/ututrust/api-sdk/connectors-backend-community.git

## Instructions

(i) Get in touch with us at our discord server:

    https://discord.gg/dFQ588vB

You can contact:

    JD (SYT) / jd_syt  
    Alfred / og1ste

(ii)  cd ~

(iii) mkdir workspace

(iv) cd workspace

(v) git clone https://gitlab.com/ututrust/api-sdk/connectors-frontend-community.git

git clone https://gitlab.com/ututrust/api-sdk/connectors-backend-community.git
    
(vi) Cut your branch from the main branch in the above 2 repositories.  The first bit of your branch 
name should be the name of the social media platform you are creating the connector for, followed 
by an underscore and your initials. For example if you are creating a connector for linkedin and 
your initials are jb, create the branch as follows:

      e.g. git checkout -b linkedin_jb

(vii) In the frontend project use react and optionally typescript.  In the backend project use 
nodejs and nestjs


(viii) Add an INSTRUCTIONS.md file to your branches in the frontend and backend repositories which 
goes through the steps of how to get your example working.  For example if you are creating a 
LinkedIn Connector you need to create a company page on linked in and configure 2 Linked in 
Application Products.  You then need to add the clientId to your .env file.

## API

Your frontend code will call a nestjs backend to retrieve the connections.

The json sent in the request is any info you need to send. It can be of any format. 

For example in the case of Linkedin the request json looks like this:

    {
      accessCode: 'asdasd324dddd'
    }

The response is should be of a fixed format and looks like:

    {
        "user": {
            "profile": {
                "firstName": "Joe",
                "lastName": "Blogs",
                "socialMediaHandle": "joe.blogs@gmail.com"
            },
            "connections": [
                {
                  "firstName": "Tom",
                  "lastName": "Dickens",
                  "socialMediaHandle": "tom.dickens@gmail.com"
                }
            ]
        }
    }

You can create TypeScript classes defining the structure of the request or response on both the 
frontend and backend or you can build the json objects directly - the choice is yours.  There are
advantages to using typed TypeScript objects but equally building up the json obects dynamically
can reduce complexity.

Note that in the above response the profile json belongs to the user who logged in to the social
media platform.  In the case of linkedin the socialMediaHandle is their email address.

However in the case of Telegram the socialMediaHandle is a telephone number.  

The connections in the response are the connection contacts of the person specified in the profile.
In the above example the loggedin user, Joe Blogs, has one connected contact called "Tom Dickens".

## Definition of Connector bounty task being "Completed"

Your Connector example is considered complete when the running of the instructions specified in 
INSTRUCTIONS.md of both the frontend and backend repos successfully allows the user to login to
the social media platform, retrieve their connections and display them on the frontend.

The code and INSTRUCTIONS.md of both the backend and frontend repositories should be of a good 
standard.

Both the frontend code and backend code should be on the a branch with the same name on both the 
frontend and backend git repositories.  The branch name should also follow the naming convention 
specified in this README. 

Note that Social Media platforms may require the opening of special dev accounts and certain 
products or applications may need to be configured. These social media configurations should be
documented in the INSTRUCTION.md of the frontend or backend repositories on your branch.

Instructions need to be included showing how to set up these dev accounts on the social media
platform.

Further a sample .env file should be provided named:

    .env_sample

.env file is not permitted to be checked into the git repository for security reasons.

## Which Social Media Connectors can be coded?

Answer: Any social media connector, except Telegram and LinkedIn which allow for the retrieval of 
connections.  Note that not all social media platforms allow for connections to be retrieved.

Therefore research is required.

Telegram is already coded.

Linkedin has temporarily discontinued allowing non partners to be granted the required permission
to retrieve connections from their Connections API.  Once they open up their programme to more 
partners, work on the Linkedin Connector could be resumed.

UTU previously had a twitter connector - however their API access changed with Elon Musk. You can
check out whether X has enabled its API to retrieve Connections or not.

Otherwise there are a lot of social media platforms out there which may or may not allow connections
to be retrieved.

It is also theoretically possible for some screen scraping software to screen scrape on behalf of 
a user to retrieve their connections - however this would need to be easy for the end user to 
trigger in an application context.  Possibly a browser extension could retrieve the connections
with the consent of a user.  However the first point of call should be to retrieve connections using
APIs if they exist.

## Note

(i) There there is already the following branch on both the frontend and backend repositories:

    linkedin_jd_bb

You can look at this for inspiration on how your connector may look like.

(ii) Do not merge your branch to the main branch.  The main branch should only have this README.md

The idea is that each example of a connector lives on its own branch.
